#!/usr/bin/python3
import sys
import html2text
from googlesearch import search
import nltk
from nltk.tokenize import word_tokenize
import requests
import urllib
import articleDateExtractor
from nameparser.parser import HumanName
import datefinder
from datetime import datetime

def get_article(keyword):
    with open('name_date_cooccurence.csv', 'a') as f:
        f.write('Name,Count,PublishedDate,OtherDates\n')
    names_list = []
    for url in search(keyword, tld='com', stop=300):
        f = requests.get(url)
        txt = html2text.html2text(f.text)
        matches = datefinder.find_dates(txt)
        keyword_txt = keyword.decode("utf-8").split(" ")[-1].lower()
        # keyword_txt = keyword.decode("utf-8").lower()
        count = txt.lower().split().count(keyword_txt)
        d = articleDateExtractor.extractArticlePublishedDate(url)
        with open('name_date_cooccurence.csv', 'a') as fh:
            fh.write(keyword_txt + ',' + str(count) + ',' + datetime.strftime(d) + ',' + ' '.join(matches) + "\n")
        
if __name__ == '__main__':
    if len(sys.argv) >= 3:
        keyword = ' '.join(sys.argv[1:])
        keyword_bytes = str.encode(keyword)
        get_article(keyword_bytes)
    else:
        print("python application.py <keyword>")
